from flask import Flask, jsonify, request, abort, session
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from status import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_409_CONFLICT
from flask_jwt_extended import jwt_required, create_access_token, create_refresh_token, get_jwt_identity
from flask_jwt_extended import JWTManager
from flask_cors import CORS, cross_origin
import psycopg2
import psycopg2.extras
import validators
from datetime import timedelta
from datetime import datetime


app = Flask(__name__)

app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:root@localhost/demo2'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config['SECRET_KEY'] = 'cairocoders-ednalan'  
app.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=10)

CORS(app)

DB_HOST = "localhost"
DB_NAME = "demo2"
DB_USER = "postgres"
DB_PASS = "root"

conn = psycopg2.connect(dbname = DB_NAME, user = DB_USER, password = DB_PASS, host = DB_HOST,) 

#conn.close()

db = SQLAlchemy(app)
jwt = JWTManager(app)



class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.Text(), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())
    contacts = db.relationship('Contact', backref="user")

    

class Contact(db.Model):
    __tablename__ = "contacts"
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text, nullable=True)
    url = db.Column(db.Text, nullable=False)
    short_url = db.Column(db.String(3), nullable=True)
    visits = db.Column(db.Integer, default=0)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())




@app.route('/register', methods=['POST'])
def register():
    username = request.json['username']
    email = request.json['email']
    password = request.json['password']

    if len(password) < 6:
        return jsonify({'error': "Password is too short"}), HTTP_400_BAD_REQUEST

    if len(username) < 3:
        return jsonify({'error': "User is too short"}), HTTP_400_BAD_REQUEST

    if not username.isalnum() or " " in username:
        return jsonify({'error': "Username should be alphanumeric, also no spaces"}), HTTP_400_BAD_REQUEST

    if not validators.email(email):
        return jsonify({'error': "Email is not valid"}), HTTP_400_BAD_REQUEST

    if User.query.filter_by(email=email).first() is not None:
        return jsonify({'error': "Email is taken"}), HTTP_409_CONFLICT

    if User.query.filter_by(username=username).first() is not None:
        return jsonify({'error': "username is taken"}), HTTP_409_CONFLICT

    pwd_hash = generate_password_hash(password)

    user = User(username=username, password=pwd_hash, email=email)
    db.session.add(user)
    db.session.commit()

    return jsonify({
        'message': "User created",
        'user': {
            'username': username, "email": email
        }

    }), HTTP_201_CREATED

    """{
    "username": "Avinash",
    "email": "avinash@gmail.com",
    "password": 12334567
}"""



@app.route('/')
def home():
    passhash = generate_password_hash('cairocoders')
    print(passhash)
    if 'username' in session:
        username = session['username']
        return jsonify({'message' : 'You are already logged in', 'username' : username})
    else:
        resp = jsonify({'message' : 'Unauthorized'})
        resp.status_code = 401
        return resp


#API Login 

@app.route('/login', methods=['POST'])
def login():
    email = request.json.get('email', '')
    password = request.json.get('password', '')

    user = User.query.filter_by(email=email).first()

    if user:
        is_pass_correct = check_password_hash(user.password, password)

        if is_pass_correct:
            refresh = create_refresh_token(identity=user.id)
            access = create_access_token(identity=user.id)

            return jsonify({
                'user': {
                    'refresh': refresh,
                    'access': access,
                    'username': user.username,
                    'email': user.email
                }

            }), HTTP_200_OK

    return jsonify({'error': 'Wrong credentials'}), HTTP_401_UNAUTHORIZED


#logout

@app.route('/logout')
def logout():
    if 'username' in session:
        session.pop('username', None)


    return jsonify({'message' : 'You successfully logged out'})





#add user entries to the database

@cross_origin()
@app.route('/addusers', methods = ['POST'])
def create_user():
    user_data = request.json

    username = user_data['username']
    email = user_data['email']
    password = user_data['password']
    created_at = user_data['created_at']

    #user_description = user_data['user_description']
    user = User(username =username, email = email, password = password, created_at =created_at)
    db.session.add(user)
    db.session.commit()
    

    return jsonify({"success": True,"response":"user added"})

"""{
    "username": "avinash",
    "email": "usertype@gmail.com",
    "password": 24,
    "created_at": "16/04/2022"
}"""


    

    #return jsonify({"success": True,"response":"user added"})
#we will use the GET method to return all the user entries

@cross_origin()    
@app.route('/getusers', methods = ['GET'])
def getusers():
     all_users = []
     users = User.query.all()
     for user in users:
          results = {
                    "user_id":user.id,
                    "username":user.username,
                    "email":user.email,
                    "password":user.password,
                    "created_at":user.created_at, }
          all_users.append(results)

     return jsonify(
            {
                "success": True,
                "users": all_users,
                "total_users": len(users),
            }
        )
#PATCH Endpoint
#We will use the PATCH method to update the details

@cross_origin()  
@app.route("/users/<int:user_id>", methods = ["PATCH"])
def update_user(user_id):
    user = User.query.get(user_id)
    username = request.json['username']
    email = request.json['email']
#Only the age and the description of the pet can be updated
    if user is None:
        abort(404)
    else:
        user.username = username
        user.email = email
        db.session.add(user)
        db.session.commit()
        return jsonify({"success": True, "response": "user Details updated"})


{
    "username": "avinash24",
    "email": "email@gmail.com"
}



if __name__ == '__main__':
  app.run(debug=True)